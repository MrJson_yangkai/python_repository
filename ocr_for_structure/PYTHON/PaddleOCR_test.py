from fastapi import FastAPI
from fastapi.responses import RedirectResponse
from paddleocr import PaddleOCR
import os
import uvicorn

# 初始化OCR
runpath = os.path.abspath(".")
ocr = PaddleOCR(
    use_angle_cls=True,
    use_gpu=False,
    det_model_dir=runpath + "/det/",  # 检测模型目录
    # det_max_side_len=960,  # 图片长边的最大尺寸,超出缩放,正常游戏截图都比较小使用默认基本都可以
    # det_db_thresh=0.3,  # 检测模型输出预测图的二值化阈值                  影响识别关键0.1-1之间自己调整
    # det_db_box_thresh=0.5,  # 检测模型输出框的阈值, 低于此值的预测框会被丢弃   影响识别关键0.1-1之间自己调整
    # det_db_unclip_ratio=2,  # 检测模型输出框扩大的比例                       影响识别关键0.1-10之间自己调整 游戏正常在0.3-2之间
    rec_model_dir=runpath + '/rec/',  # 识别模型目录
    # rec_char_dict_path=runpath + '/rec/dict.txt',  # 识别字典文件
    use_space_char=True,  # 是否识别空格
    # max_text_length=25,  # 识别的最大文字长度
    cls_model_dir=runpath + '/cls/',  # 分类模型目录 正常用不到
    # use_gpu=False,  # 使用GPU
    lang="ch",  # 模型语言类型,目前支持 中文(ch)、英文(en)及数字  ch=中文+数字+英文
    # det=True,  # 使用启动检测
    # rec=True,  # 是否启动识别
    # cls=False,  # 是否启动分类
)

app = FastAPI()


@app.get('/ocr/{img}')
async def myocr(img):
    try:
        result = ocr.ocr(img, det=True, cls=False)
        retrec = {}
        for k, v in result:
            text = v[0]  # 识别到的文字
            score = '{:.3f}'.format(v[1])  # 识别准确率 保留小数点后三位
            top = str(int(k[0][0])) + "," + str(int(k[0][1]))  # 识别文本相对图片顶点坐标
            bot = str(int(k[2][0])) + "," + str(int(k[2][1]))  # 识别文本相对图片下边坐标
            retrec[text] = {'top': top, 'bot': bot, 'score': score}
        return str(retrec)
    finally:
        pass



if __name__ == '__main__':
    img_path = '111.png'
    result = ocr.ocr(img_path, cls=True)
    for line in result:
        print(line[1][0])