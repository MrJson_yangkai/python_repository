# coding: utf8
import requests
import json
import cv2
import base64

def cv2_to_base64(image):
    data = cv2.imencode('.jpg', image)[1]
    return base64.b64encode(data.tostring()).decode('utf8')

# 发送HTTP请求
data = {'images':[cv2_to_base64(cv2.imread("./images/111.png"))]}
print(data)
headers = {"Content-type": "application/json"}
# url = "http://47.98.43.31:8866/predict/ocr_system"
# url = "http://47.98.43.31:8866/predict/ocr_rec"
url = "http://47.98.43.31:8860/predict/chinese_text_detection_db_server"
# url = "http://47.95.218.160:8000/predict/ocr_system"
# url = "http://47.95.218.160:8080/predict/ocr_system"
r = requests.post(url=url, headers=headers, data=json.dumps(data))

# 打印预测结果
print(r.json()["results"])

# print(len(r.json()["results"][0]))
# print(r.json()["results"][0])
# print(r.json()["results"][0][0]["text_region"][0][0])
# print(r.json()["results"][0][0]["text_region"][0][1])
# # print(r.json()["results"][0][0]["text"])
# # print(r.json()["results"][0][0])
# datas = r.json()["results"][0]
# for data in datas:
#     # print(data)
#     if data['text'].startswith("附近") and data['text'].endswith("人已下单"):
#         print(data['text'])
#     if data['text'].startswith("￥"):
#         print(data['text'])
# print(r.json())

# hub serving start -m chinese_text_detection_db_server -p 8866
# hub serving start -m chinese_ocr_db_crnn_mobile -p 8866
# hub serving start -m ocr_system
# hub serving start -m chinese_ocr_db_crnn_mobile -p 8086